export class MyMessageUpdated {
  public readonly data: string;
  constructor(_data: string) {
    this.data = _data;
  }
}
