import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-datepicker-webpack/dist/css/bootstrap-datepicker3.css';
import 'bootstrap-datepicker-webpack/dist/locales/bootstrap-datepicker.sv.min.js';
import 'font-awesome/css/font-awesome.css';
import 'less/app.less';
import 'aurelia-dialog/styles/output.css';
import {Router, RouterConfiguration} from 'aurelia-router';
import {PLATFORM} from 'aurelia-pal';

export class App {
  private router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Aurelia Template';
    config.options.pushState = true;
    config.map([
      { route: '', redirect: 'start'},
      { route: 'start', name: 'start', moduleId: PLATFORM.moduleName('pages/start'), title: 'Start' },
      { route: 'bootstrap', name: 'bootstrap', moduleId: PLATFORM.moduleName('pages/bootstrap'), title: 'Bootstrap' },
      { route: 'translations', name: 'translations', moduleId: PLATFORM.moduleName('pages/translations'), title: 'Translations' },
      { route: 'webapi', name: 'webapi', moduleId: PLATFORM.moduleName('pages/webapi'), title: 'WebApi' },
      { route: 'myform', name: 'myform', moduleId: PLATFORM.moduleName('pages/myform'), title: 'My Form' },
      { route: 'highlightjs', name: 'highlightjs', moduleId: PLATFORM.moduleName('pages/highlightjs'), title: 'Highlightjs' },
      { route: 'tables', name: 'tables', moduleId: PLATFORM.moduleName('pages/tables'), title: 'Tables' }
    ]);
    this.router = router;
  }
}
