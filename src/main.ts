import 'whatwg-fetch';
import * as Bluebird from 'bluebird';
import * as Backend from 'i18next-xhr-backend';
import { Aurelia } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';
import { LogManager } from 'aurelia-framework';
import { ConsoleAppender } from 'aurelia-logging-console';

Bluebird.config({ warnings: { wForgottenReturn: false } });
LogManager.addAppender(new ConsoleAppender());
LogManager.setLevel(LogManager.logLevel.debug);

export async function configure(aurelia: Aurelia) {
    aurelia.use.standardConfiguration();

    //
    // Plugins
    //

    aurelia.use.plugin(PLATFORM.moduleName('aurelia-configuration'), (config: any) => {
        config.setDirectory('static');
        config.setConfig('config.json');
        config.setEnvironments({
            development: ['localhost', '127.0.0.1'],
            staging: ['staging.website.com', 'test.staging.website.com'],
            production: ['website.com']
        });
    });

    aurelia.use.plugin(PLATFORM.moduleName('aurelia-i18n'), (instance: any) => {
        let aliases = ['t', 'i18n'];
        instance.i18next.use(Backend);
        return instance.setup({
          backend: {
            loadPath: './static/locales/{{lng}}/{{ns}}.json',
          },
          attributes: aliases,
          lng : 'sv',
          fallbackLng : 'en',
          debug : false,
          ns: ['translation'],
          defaultNS: 'translation'
        });
    });

    aurelia.use.plugin(PLATFORM.moduleName('aurelia-dialog'));
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-validation'));
    aurelia.use.plugin(PLATFORM.moduleName('au-table'));

    //
    // End Plugins
    //

    //
    // Global Resources
    //

    aurelia.use.globalResources(
      PLATFORM.moduleName('compose/banner'),
      PLATFORM.moduleName('compose/spot'),
      PLATFORM.moduleName('compose/null')
    );

    //
    // End Global Resources
    //

    await aurelia.start();
    await aurelia.setRoot(PLATFORM.moduleName('app'));
}
