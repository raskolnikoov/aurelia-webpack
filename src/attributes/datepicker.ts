import {inject, customAttribute} from 'aurelia-framework';

@customAttribute('datepicker')
@inject(Element)
export class DatePicker {

  constructor(private element: Element) {
  }

  private attached(): void {
    $(this.element).datepicker().on('change', (e: any) => {
      this.fireEvent(e.target, 'input');
      this.fireEvent(e.target, 'changed');
    });
  }

  private detached(): void {
    $(this.element).datepicker('destroy').off('change');
  }

  private createEvent(name: string): Event {
    var event = document.createEvent('Event');
    event.initEvent(name, true, true);
    return event;
  }

  private fireEvent(element: Element, name: string): void {
    var event = this.createEvent(name);
    element.dispatchEvent(event);
  }
}
