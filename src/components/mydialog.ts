import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';

@inject(DialogController)
export class MyDialog {

  private model: any;

  constructor(private controller: DialogController) {
    this.model = {};
  }

  //All of the parameters that we passed to the dialog are available through the model
  private activate(startName: string): void {
      this.model.name = startName;
  }

  private ok(): void {
      this.controller.ok(this.model.name);
  }

  private cancel(): void {
      this.controller.cancel();
  }
  
}
