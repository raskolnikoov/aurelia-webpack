import {MyMessageUpdated} from '../messages';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';

@inject(EventAggregator)
export class HeaderBar {

  private myMessage: string = 'Typescript App';

  constructor(private ea: EventAggregator) {
    ea.subscribe(MyMessageUpdated, (msg: any) => {
      this.myMessage = msg.data;
    });
  }

}
