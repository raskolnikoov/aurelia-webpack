import {inject} from 'aurelia-framework';
import {ComposeService} from '../services/compose-service';

@inject(ComposeService)
export class Spot {

  private model: any;
  private components: any = [];

  constructor(private cs: ComposeService) {

  }

  private activate(private data: any): void {
    this.model = data;
    let request = this.model.placeholder_left;
    console.log('placeholder_left', request);
    this.components = this.cs.composeModule(request);
  }
}
