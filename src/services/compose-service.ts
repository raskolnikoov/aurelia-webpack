export class ComposeService {

  public composeModule(private request: any): any {

    console.log('cs-request', request);

    let components = [];
    for (var req of request) {
      let item = {
        type: this.createModule(req.type),
        data: req.data
      }
      components.push(item);
    }
    return components;
  }

  private createModule(private type: any): any {
    switch(type) {
      case 'a':
        return 'compose/banner';
      case 'b':
        return 'compose/spot';
      default:
        return 'compose/null';
    }
  }
}
