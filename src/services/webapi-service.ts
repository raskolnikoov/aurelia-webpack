import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';

@inject(HttpClient)
export class WebAPIService {

  constructor(private client: HttpClient){

  }

  public async getDataAsync(): Promise<string> {
    let data = await this.client.get('http://api.soderberg.co/list/persons');
    return JSON.parse(data.response);
  }
}
