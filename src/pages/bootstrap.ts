import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';

@inject(I18N)
export class Bootstrap {

  private startDate: string = '2015-01-01';
  private lang: string;
  private fromDate: string = '2017-08-08';
  private toDate: string = '2017-08-10';

  constructor(private i18n: I18N) {
    this.lang = this.i18n.getLocale(); //sv
  }

  private datesUpdated(): void {
    console.log('fromDate', this.fromDate);
    console.log('toDate', this.toDate);
  }
}
