interface IUser {
  name: string;
  age: number;
  email: string;
  registered: string;
  isActive: boolean;
}

interface IFilter {
  value: string;
  keys: Array<string>;
}

export class Tables {

  private users: Array<IUser> = [];
  private filters: Array<IFilter> = [];

  constructor() {

    this.users = [
      {
        name: 'Daniel Söderberg',
        age: 32,
        email: 'daniel@kompilera.se',
        registered: '1985-03-23',
        isActive: true
      },
      {
        name: 'Caroline Paulander',
        age: 33,
        email: 'c_paulander@hotmail.com',
        registered: '1984-09-10',
        isActive: true
      },
      {
        name: 'Hugo Söderberg',
        age: 1,
        email: 'hugo@kompilera.se',
        registered: '2016-08-08',
        isActive: false
      }
    ];

    this.filters = [
        {
          value: '',
          keys: ['name', 'age', 'email', 'registered']
        }
    ];
  }

  private nameLength(row: IUser): number {
      return row.name.length;
  }

  private dateSort(a: IUser, b: IUser, sortOrder: number): number {
      let date1 = new Date(a.registered);
      let date2 = new Date(b.registered);

      if (date1 === date2) {
          return 0;
      }

      if (date1 > date2) {
          return 1 * sortOrder;
      }

      return -1 * sortOrder;
  }
}
