import {inject} from 'aurelia-framework';
import {WebAPIService} from '../services';

@inject(WebAPIService)
export class WebApi {

  private output: any;

  constructor(private api: WebAPIService) {
  }

  private async btnGet(): Promise<void> {
    var result = await this.api.getDataAsync();
    console.log('api data', result);
    this.output = result;
  }

}
