import {MyMessageUpdated} from '../messages';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject, LogManager} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {DialogService} from 'aurelia-dialog';
import {MyDialog} from '../components/mydialog';
import {log} from '../log';
import {ComposeService} from '../services/compose-service';

@inject(EventAggregator, AureliaConfiguration, DialogService, ComposeService)
export class Start {

  private title: string = 'Start Page';
  private name_label: string;
  private key_label: string;
  private endpoint_label: string;
  private inputtext: string;
  private components: any = [];

  constructor(
    private ea: EventAggregator,
    private ac: AureliaConfiguration,
    private ds: DialogService,
    private cs: ComposeService) {

    this.name_label = this.ac.get('name');
    this.key_label = this.ac.get('api.key');
    this.endpoint_label = this.ac.get('api.endpoint');

    log.debug('debug.daniel');
    log.info('info.daniel');
    log.warn('warn.daniel');
    log.error('error.daniel');
  }

  private async send(): Promise<void> {
    this.ea.publish(new MyMessageUpdated(this.inputtext));
    this.inputtext = '';
  }

  private openMyDialog(): void {
    this.ds.open({
      viewModel: MyDialog,
      model: 'hugo'
    }).whenClosed(response => {
      console.log(response.output);
    });
  }

  private async loadCompose(): Promise<void> {

    let request = [
      {
        type: 'a',
        data: {
          slogan: "let's do it"
        }
      },
      {
        type: 'b',
        data: {
          title: "Aurelia",
          image: "http://aurelia.io/styles/images/logo.svg",
          placeholder_left: [
            {
              type: 'a',
              data: {
                slogan: 'first'
              }
            },
            {
              type: 'a',
              data: {
                slogan: 'second'
              }
            }
          ]
        }
      }
    ];

    this.components = this.cs.composeModule(request);
  }

}
