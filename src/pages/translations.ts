import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';

@inject(I18N)
export class Translations {

  constructor(private i18n: I18N) {
    console.log('current locale:', this.i18n.getLocale());
    console.log('backend text:', this.i18n.tr('sample-text'));
  }

  private async changeLocale(locale: string): Promise<void> {
    await this.i18n.setLocale(locale);
    console.log('current locale:', this.i18n.getLocale());
    console.log('backend text:', this.i18n.tr('sample-text'));
  }
}
