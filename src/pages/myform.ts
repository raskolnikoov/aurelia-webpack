import {inject} from 'aurelia-framework';
import {
  ValidationControllerFactory,
  ValidationRules
} from 'aurelia-validation';

@inject(ValidationControllerFactory)
export class MyForm {

  private controller: any;
  private name: string = '';
  private email: string = '';
  private age: number = null;

  constructor(controllerFactory: ValidationControllerFactory) {

    this.controller = controllerFactory.createForCurrentScope();

    ValidationRules
      .ensure('age')
        .matches(/^\d+$/)
          .withMessage('måste vara en siffra')
        .required()
          .withMessage('ålder krävs')
      .ensure('name')
        .minLength(3)
          .withMessage('mer än 2 tecken')
        .maxLength(10)
          .withMessage('mindre än 11 tecken')
        .required()
          .withMessage('namn krävs')
      .ensure('email')
        .email()
          .withMessage('ej rätt email')
        .required()
          .withMessage('email krävs')
    .on(this);
  }

  private async send(): Promise<void> {
    let result = await this.controller.validate();
  }

}
