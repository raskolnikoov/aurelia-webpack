Installation:

- npm install yarn webpack webpack-dev-server -g
- yarn install

To run application:

- yarn start

To bundle application:

- webpack --env.production
