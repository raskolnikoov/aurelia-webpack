const path = require('path')
const { CheckerPlugin } = require('awesome-typescript-loader')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const { AureliaPlugin } = require('aurelia-webpack-plugin');
const { optimize: { CommonsChunkPlugin, UglifyJsPlugin }, ProvidePlugin } = require('webpack');

const outDir = path.resolve(__dirname, 'dist');
const srcDir = path.resolve(__dirname, 'src');
const nodeModulesDir = path.resolve(__dirname, 'node_modules');
const baseUrl = '/';

module.exports = function(env) {

  console.log('Running Webpack with ENV args:', env);

  const config = {
      entry: {
          'app': path.join(srcDir, 'main.ts'),
          'aurelia': [
            'aurelia-bootstrapper',
            'aurelia-highlightjs',
            'au-table'
          ],
          'libs': [
            'jquery',
            'whatwg-fetch',
            'mutationobserver-shim',
            'raf',
            'bluebird',
            'intl',
            'i18next-xhr-backend',
            'array-includes',
            'moment',
            'signalr',
            'highlight.js',
            'bootstrap',
            'bootstrap-datepicker-webpack'
          ]
      },
      output: {
        path: outDir,
        publicPath: baseUrl,
        filename: '[name].js',
        sourceMapFilename: '[name].bundle.map',
        chunkFilename: '[name].js'
      },
      resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        modules: [srcDir, 'node_modules'],
        alias: {
          'jquery': path.join(nodeModulesDir, 'jquery/src/jquery'),
          'signalr': path.join(nodeModulesDir, 'signalr/jquery.signalr.js')
        }
      },
      module: {
        rules: [
          //{ test: /\.less$/i, use: ['style-loader', 'css-loader', 'less-loader'] },
          //{ test: /\.css$/, loader: 'style-loader!css-loader' },
          {
            test: /\.less$/i,
            use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: ['css-loader', 'less-loader']
            })
          },
          {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: 'css-loader'
            })
          },
          { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
          { test: /\.html$/i, loader: 'html-loader' },
          { test: /\.(png|jpg|jpeg|gif)$/i, loader: 'url-loader?limit=8192' },
          { test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/i, loader: 'url-loader', options: { limit: 10000, mimetype: 'application/font-woff2' } },
          { test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/i, loader: 'url-loader', options: { limit: 10000, mimetype: 'application/font-woff' } },
          { test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/i, loader: 'file-loader' },
          { test: /\.json$/, loader: 'ignore-loader' }
        ]
      },
      plugins: [
          new CheckerPlugin(),
          new CleanWebpackPlugin(['dist']),
          new AureliaPlugin(),
          new ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'Promise': 'bluebird',
            'moment': 'moment',
            'hljs': 'highlight.js'
          }),
          new CopyWebpackPlugin([
            {
              from: path.resolve(srcDir, 'static'),
              to: 'static'
            },
            {
              from: path.resolve(srcDir, 'Web.config'),
              to: 'Web.config'
            },
            {
              from: path.resolve(srcDir, 'robots.txt'),
              to: 'robots.txt'
            },
            {
              from: path.resolve(srcDir, 'favicon.ico'),
              to: 'favicon.ico'
            }
          ]),
          new CommonsChunkPlugin({ name: ['aurelia', 'libs'] }),
          new ExtractTextPlugin('styles.css'),
          new HtmlWebpackPlugin({
            template: path.resolve(srcDir, 'index.html'),
            minify: {
              removeComments: true,
              collapseWhitespace: true
            }
          })
      ],
      devServer: {
        port: 3000,
        contentBase: outDir,
        open: true,
        historyApiFallback: true
      }
  };

  if(env === undefined) return config;

  if (env.production) {

    console.log('Run Production tasks..');

    config.plugins.push(
      new UglifyJsPlugin({
        compress: { warnings: false },
        output: { comments: false },
        sourceMap: false,
        include: ['app.js'],
        exclude: ['aurelia.js', 'libs.js']
      })
    );

  }

  return config;
};
